window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url)
    if (response.ok){
        const data = await response.json();
        console.log(data)
        const selectTag = document.getElementById('location');
        for (let location of data.location){
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.id;

            selectTag.appendChild(option);
        }
    }
});
