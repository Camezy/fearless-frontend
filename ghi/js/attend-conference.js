window.addEventListener('DOMContentLoaded', async() => {
    const url = 'http://localhost:8001/api/conferences/1/attendees/'

    const response = await fetch(url)
    if (response.ok){
        const data = await response.json();
        console.log(data);
        const selectTag = document.getElementById('conference');
        for(let conference in data.conference){
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.href;

            selectTag.appendChild(option);
        }
    }
})
