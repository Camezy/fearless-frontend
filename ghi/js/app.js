function createAlert(warning){
    return `
    <div class="alert alert-danger d-flex align-items-center" role="alert">
        <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:"><use xlink:href="#exclamation-triangle-fill"/></svg>
        <div>
            <strong>${warning}</strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    `;
}




window.addEventListener('DOMContentLoaded', async() => {





    function createCard(title, description, pictureUrl, starts, ends, name) {
        return `
        <div class="col">
          <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${title}</h5>
              <p class="card-subtitle mb-2 text-muted">${name}</p>
              <p class="card-text">${description}</p>
              <p class="card-text">${starts}-${ends}</p>
            </div>
          </div>
          </div>
        `;
      }
    //   So add some affect on the text is to make sure the section of HTML is removed from the Index Line 46. then
    // from there, use Bootstrap for that subtitle and copy that class stuff and paste it into the
    // HTML section that i wanted it to apply to. I.e Line 12










    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!response.ok){
            // Figure out what to do when the response is bad
            const warning = "Bad Response!"
            const html = createAlert(warning, "warning");
            const errorTag = document.querySelector("#error");
            errorTag.innerHTML = html
        } else {
            const data = await response.json();

            // Here the conference[0] stuff was deleted and got removed since its not being used
            // const nameTag = document.querySelector('.card-title');
            // // nameTag.innerHTML = conference.name;

            for(let conference of data.conferences){

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if(detailResponse.ok){
                    // Details is the head of the tree then file down
                    const details = await detailResponse.json();
                    // Below is an alertative
                    // **** const description = details.conference.description; ****
                    // **** const image = details.conference.location.picture_url ****
                    // const titleTag = document.querySelector('.card-title')

                    // const imageTag = document.querySelector('.card-img-top')
                    // const nameTag = document.querySelector('.card-text');
                    // Below are easier ways to write the code and go straight to the source
                    // This is what it will look like if i did not stream line like

                    // How i did did below:
                    //  nameTage.innerHTML = description
                    // imageTag.src = image

                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const title =  details.conference.name;
                    const name = details.conference.location.name;



                    // Like the below
                    //So when modifing the date, you can add this into the If statement to get things going
                    // from here i have to make sure that i add these varaibles to both the
                    // const HTML and the CreateCard function to makesure the Computer knows where the Information
                    // Is coming from. Then also add the ${}${} to grab the stuff and place
                    // the into the HTML

                    const starts = new Date(details.conference.starts).toLocaleDateString()
                    const ends = new Date(details.conference.ends).toLocaleDateString()

                    // innerHTML text and src image grabber

                    const html = createCard(title, description, pictureUrl, starts, ends, name);
                    console.log(html);

                    const row = document.querySelector('.row');
                        row.innerHTML += html;
                        // This was originally Columns so what we
                        // was change it to "rows" so that it
                        // matches the picture of mutliple coloumns
                        // also greyed out the "col" html to see it in full affect
                        // Also, what we did next was go to the
                        // create card and added another div to wrap the code in
                        // and placed "col" in that and came out to the changes we have now.
                        // also in this function above i changed the cols to rows so that it
                        // displays the way it does




                }
            }

        }
    }   catch (e) {

        console.log(e)
        // Figure out what to do if an error is raised
        const html = createAlert(warning, "warning");
        const errorTag = document.querySelector('#error');
        errorTag.innerHTML = html















    }

});


// So when trying to insert data into the HTML similar to what is coded above
// if the HTML files are not deleted we can do something like this
// nameTag.innerHTML = details.conference.name
// imageTag.src = details.conference.location.picture_url (Also make sure its in the list encoder or)
// titleTag.innerHTML = details.conference.name

// Basically this is the idea of it
