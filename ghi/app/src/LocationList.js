import {useEffect, useState} from 'react'

function LocationList(){
    const [locations, setLocations] = useState([]);

    const getData = async () => {
        const request = fetch('http://localhost:8000/api/locations/')

        const response = await request.json()

        setLocations(response.locations)
    }

    useEffect(() =>{
        getData();
   }, [])

   return(
    <div>
            <h1>List Locations</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    {
                        locations.map(conference => {
                            return(<tr key={conference.href}>
                                <td>{conference.name}</td>
                            </tr>);
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}
export default LocationList;
