import React, {useEffect, useState } from 'react';

function AttendeeForm(props) {
    // Based on the POST/CREATE In insonmia on what is needed in JSON
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [conference, setConference] = useState("");
    // Now the list of conferences
    const [conferences, setConferences] = useState([]);




    const handleNameChange = (event) =>{
        const value = event.target.value;
        setName(value);
    }

    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }




    const handleSubmit = async (event) =>{
        const value = event.target.value;
        event.preventDefault();

        const data = {};

        data.name = name;
        data.email = email;
        data.conference = conference;

        const attendeeUrl = 'http://localhost:8001/api/conferences/1/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(attendeeUrl, fetchConfig);
        if(response.ok){
            const newAttendee = await response.json();
            console.log(newAttendee);

            setName("");
            setEmail("");
            setConference("");
        }
    };



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        console.log(url)
        const response = await fetch(url);
        // Now grabbing the FK, the pluarl set
        if(response.ok){
            const data = await response.json();
            setConferences(data.conferences);
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return(
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleEmailChange} value={email} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" />
                        <label htmlFor="room_count">Email</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleConferenceChange} value={conference} required name="state" id="state" className="form-select" >
                            <option value="">Conferences</option>
                            console.log(conferences)
                            {/* This is the list of the locations needs the pluarl/ array version, needs two for the drop down menus*/}
                            {conferences.map(conference => {
                                return (
                                    <option key={conference.href} value={conference.href}>{conference.name}</option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}

export default AttendeeForm;
