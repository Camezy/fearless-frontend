import { Routes, Route, Link, BrowserRouter} from "react-router-dom";

import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from "./PresentationForm";
import ConferenceList from "./ConferenceList";
import LocationList from "./LocationList";
import MainPage from "./MainPage";


export default function App(props) {
  return (
    <BrowserRouter>
      <div className="container">
      <Nav />
      <Routes>
        {/* Grab */}
          <Route path="/" element={<MainPage />}/>
          <Route path="/conferences/new" element={<ConferenceForm />}/>
          <Route path="/conferences" element={<ConferenceList />}/>

          <Route path="/attendees" element={<AttendeesList />}/>
          <Route path="/attendees/new" element={<AttendConferenceForm />}/>


          <Route path="/locations/new" element={<LocationForm />}/>
          <Route path="/locations" element={<LocationList />}/>


          {/* <Route path="/presentations" element={<PresentationForm />}/> */}


      </Routes>
      </div>
    </BrowserRouter>
  );
}
