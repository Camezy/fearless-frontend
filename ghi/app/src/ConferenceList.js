import {useEffect , useState } from 'react';

function ConferenceList(){
    const [conferences, setConferences] = useState([]);

    const getData = async () =>{
        const request = await fetch('http://localhost:8000/api/conferences/');
        if (Response.ok){
            const response = await request.json()
            setConferences(response.conferences)
        }
    }

    useEffect(() => {
        getData();
    }, [])


    const handleDelete = async(id) => {
        const request = await fetch(`http://localhost:8000/api/conferences/${id}`,
         { method: "DELETE"})
         const response = await request.json()
         getData();
    }
    return (
        <div>
            <h1>List Conference</h1>
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Name</th>
                    <th>Max Attendees</th>
                    <th>Location</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    {
                        conferences.sort((a,b)=> (a.id - b.id)).map(conference => {
                            return(<tr key={conference.href}>
                                <td>{conference.id}</td>
                                <td>{conference.name}</td>
                                <td>{conference.max_attendees}</td>
                                <td>{conference.location.name}</td>
                                <td><button onClick={()=> {
                                    handleDelete(conference.id)
                                }} className="btn btn-danger">Delete</button></td>
                            </tr>);
                        })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default ConferenceList;
